package idv.td.service;

import idv.td.been.Test1;

public interface HellowService {
	
	String getHellow();
	
	String getHellow(String name);
	
	Long insertTest1(Test1 test1);
	
	String toAddBatch();
	
}
