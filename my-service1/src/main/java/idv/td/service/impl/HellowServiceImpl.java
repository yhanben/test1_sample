package idv.td.service.impl;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import idv.td.been.Test1;
import idv.td.mapper.Test1Mapper;
import idv.td.service.HellowService;

@Service("hellowService")
@Transactional
public class HellowServiceImpl implements  HellowService{

	//@Autowired(required=false)
	@Autowired
	private Test1Mapper test1Dao;
	
	@Override
	public String getHellow() {
		System.out.println("service comming");
		
		List<Test1> allt1 = test1Dao.findAllUser();
		
		if(allt1 != null && allt1.size() > 0) {
			allt1.stream().forEach(t -> System.out.println(t.getName()));
		}else {
			System.out.println("sorry no value");
		}
		
		return "yes";
	}

	@Override
	public String getHellow(String name) {
		System.out.println("service name:" + name);
		
		Test1 t =  test1Dao.findUserByName(name);
		
		if(t != null) {
			System.out.println("phone:" + t.getPhone());
		}else {
			System.out.println("sorry no value for name");
		}
		
		return "hello:" + name;
	}

	@Override
	public Long insertTest1(Test1 test1) {
		
		Long result = test1Dao.insertTest1(test1);
		
		System.out.println("insert result:" + result);		
				
		return result;
	}

	@Override
	public String toAddBatch() {
		Connection conn = null;

		Statement st = null;

		ResultSet rs = null;

		try {
			/*
		conn = JdbcUtil.getConnection();

		String sql1 = "insert into user(name,password,email,birthday)";

		sql1 +=  "values('kkk','123','abc@sina.com','1978-08-08')";

		String sql2 = "update user set password='123456' where id=3";

		st = conn.createStatement();

		st.addBatch(sql1);  //把SQL语句加入到批命令中

		st.addBatch(sql2);  //把SQL语句加入到批命令中

		st.executeBatch();
			*/
		} finally{

		  //JdbcUtil.free(conn, st, rs);

		}
		
		return null;
	}
	
	

}
