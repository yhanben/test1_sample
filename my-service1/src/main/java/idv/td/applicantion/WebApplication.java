package idv.td.applicantion;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@SpringBootApplication(scanBasePackages = "idv.td")
@MapperScan("idv.td.mapper")
//@ImportResource("classpath:spring/application.xml")
@ImportResource(value = { "classpath:application-bean.xml" })
public class WebApplication {
	
	/*
	public static void main(String[] args) {
		System.out.println("printService");
        SpringApplication.run(WebApplication.class, args);
    }
    */
}
