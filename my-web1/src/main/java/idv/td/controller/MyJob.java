package idv.td.controller;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import idv.td.service.HellowService;

public class MyJob implements Job {


	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private HellowService hellowService;
	
	public HellowService getHellowService() {
		return hellowService;
	}

	public void setHellowService(HellowService hellowService) {
		this.hellowService = hellowService;
	}




	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("HelloCotrole111");
		System.out.println(new Date());
		//System.out.println(arg0.get)
		HellowService hellos = (HellowService)context.getBean("hellowService");
		//context.getApplicationName()
		
		System.out.println(hellos.getHellow());
		
	}
	

}
