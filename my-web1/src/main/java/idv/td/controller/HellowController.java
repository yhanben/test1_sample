package idv.td.controller;

import java.util.TimeZone;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import idv.td.been.Test1;
import idv.td.service.HellowService;
import idv.td.utility.ValidateUtil;
import idv.td.vo.UserInputVo;

@RestController
@RequestMapping("/hello")
public class HellowController {
	
	//static final Logger logger = LoggerFactory.getLogger(HellowController.class.getName());
	
	//static final Logger logger = LogManager.getLogger(HellowController.class.getName());
	
	Logger logger = LogManager.getLogger(HellowController.class.getName());
	//private static final Logger LOGGER = LogManager.getLogger("mylog");
	
	@Autowired
	private HellowService hellowService;
	
	public HellowService getHellowService() {
		return hellowService;
	}

	public void setHellowService(HellowService hellowService) {
		this.hellowService = hellowService;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
    public String sayHelloWorld() {
        
        System.out.println("sayHelloWorld");
        
        System.out.println("is blank:" +  ValidateUtil.StringIsBlank("1111"));
        return hellowService.getHellow();
    }
	
	//@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	@RequestMapping(value="/{name}", method = RequestMethod.GET)
	public String sayHello(@PathVariable String name) {
		
		try {
			JobDetail job = JobBuilder.newJob(MyJob.class).build();
			// 簡單任務
			Trigger t1 = TriggerBuilder.newTrigger().withIdentity("mySimpleTrigger").startNow().build();
			// 每天12:47分執行
			Trigger t2 = TriggerBuilder.newTrigger().withIdentity("myCronTrigger").withSchedule(CronScheduleBuilder.cronSchedule("0 47 12 1/1 * ? *")).build();
			// 每隔5秒執行
			Trigger t3 = TriggerBuilder.newTrigger().withIdentity("myCronTrigger").
			withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(5).repeatForever()).build();
			// 每天12:47(GMT 8)執行, 注意使用了inTimeZone。如果schedule是根據客戶時區安排的，就非常有用，順帶解決了DSTSaving的問題
			Trigger t4 = TriggerBuilder.newTrigger().withIdentity("myCronTrigger").withSchedule(CronScheduleBuilder.cronSchedule("0 47 12 1/1 * ? *").inTimeZone(TimeZone.getTimeZone("GMT 8"))).build();
			// 通過schedulerFactory獲取一個排程器 
			Scheduler sc = StdSchedulerFactory.getDefaultScheduler();
			// 把作業和觸發器註冊到任務排程中
			sc.scheduleJob(job, t3);
			sc.start();
		}catch(SchedulerException se) {
			System.out.println("sch Ex");
		}
		
		
		logger.info("name:" + name);
		logger.info("comming");
		logger.error("123");
		logger.error("321");
		logger.debug("555");
		logger.debug("666");
        System.out.println("to sayHello name:" + name);
        return hellowService.getHellow(name);
    }
	
	@RequestMapping(value = "", method = RequestMethod.POST)
    public String updateHello(@Valid @RequestBody UserInputVo user, BindingResult result) {
        
    	if(result.hasErrors()) {
    		/*
    		List<ObjectError> ls=result.getAllErrors();
            for (int i = 0; i < ls.size(); i++) {
                System.out.println("error:"+ls.get(i));
            }
            */
    		for (Object object : result.getAllErrors()) {
    		    if(object instanceof FieldError) {
    		        FieldError fieldError = (FieldError) object;

    		        System.out.println(fieldError.getCode());
    		        System.out.println(fieldError.getDefaultMessage());
    		    }

    		    if(object instanceof ObjectError) {
    		        ObjectError objectError = (ObjectError) object;

    		        System.out.println(objectError.getCode());
    		    }
    		}
    		
    		
    		return "oh~ you get args error";
    	}
    	
    	Test1 test = new Test1();
    	test.setID(Long.parseLong(user.getAge()));
    	test.setName(user.getUserName());
    	test.setPhone(user.getPassworld());
    	
    	Long inserResult = hellowService.insertTest1(test);
    	
    	System.out.println("sayHello: name=" + user.getUserName() + ",passworld =" + user.getPassworld() + ", age=" + user.getAge());
        return "Hello, " +  user.getUserName() + " " + user.getPassworld();
    }
	
}
