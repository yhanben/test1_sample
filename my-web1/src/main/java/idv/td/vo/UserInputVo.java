package idv.td.vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import idv.td.annotation.CustIdLength;


//@Entity
public class UserInputVo {

	//CustomObjectGroup.Update.class;
	
	@NotNull
	//@NotBlank( message = "userNmae not to empty" )
	private String userName;
	
	//@NotNull
	//@NotBlank( message = "passworld not to empty" )
	@CustIdLength
	private String passworld;
	
	@NotNull
	//@NotBlank( message = "age not to empty" )
	private String age;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassworld() {
		return passworld;
	}

	public void setPassworld(String passworld) {
		this.passworld = passworld;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
	
}
