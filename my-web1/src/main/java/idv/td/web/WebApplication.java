package idv.td.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = "idv.td")
//@ImportResource("classpath:spring/application.xml")
@MapperScan("idv.td.mapper")
@ImportResource(value = { "classpath:application-bean.xml" })
public class WebApplication {
	public static void main(String[] args) {
		System.out.println("printController");
        SpringApplication.run(WebApplication.class, args);
    }
}
