package idv.td.intecepter;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect
//@Component
public class Audience {
    
    //@Pointcut("@annotation(javax.ws.rs.Path)")
	//@Pointcut("execution(* com.tproject1.service.Performance.perform(..))")
	//@Pointcut("execution(* com.tproject1.PaymentController.*(..))")
	@Pointcut("execution(* idv.td.controller.*.*(..))")
	public void performance(){}
    
    @Before("performance()")
    public void takeSeats() {
    	System.out.println("aaaaa");
    }
    
    //@Around(value = "performance()")
    @Around("performance()")
    public Object  watchPerformance(ProceedingJoinPoint jp){
    	Object result = null;
    	try{
    		System.out.println("Audience comming");
            //System.out.println("123");
            //System.out.println("321");
            result = jp.proceed();
            System.out.println("process close");
            
        }catch(Throwable e){
            System.out.println("error:" + e);
        }
		return result;
    }
    
    /*
    @Pointcut(value = "execution(* com.tproject1.PaymentController.sayHello(..))")
    //@Pointcut("@annotation(javax.ws.rs.Path)")
    public void performance(){}
    
    
    @Around(value = "performance()")
    public void watchPerformance(ProceedingJoinPoint jp){
        try{
            System.out.println("123");
            System.out.println("321");
            jp.proceed();
            System.out.println("process close");
            
        }catch(Throwable e){
            System.out.println("error");
        }
    }
    */
    
}

