package idv.job;

import java.util.TimeZone;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import idv.td.controller.MyJob;

public class MyScheduler {
	public static void main(String[] args) throws SchedulerException {
		JobDetail job = JobBuilder.newJob(MyJob.class).build();
		// 簡單任務
		Trigger t1 = TriggerBuilder.newTrigger().withIdentity("mySimpleTrigger").startNow().build();
		// 每天12:47分執行
		Trigger t2 = TriggerBuilder.newTrigger().withIdentity("myCronTrigger").withSchedule(CronScheduleBuilder.cronSchedule("0 47 12 1/1 * ? *")).build();
		// 每隔5秒執行
		Trigger t3 = TriggerBuilder.newTrigger().withIdentity("myCronTrigger").
		withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(5).repeatForever()).build();
		// 每天12:47(GMT 8)執行, 注意使用了inTimeZone。如果schedule是根據客戶時區安排的，就非常有用，順帶解決了DSTSaving的問題
		Trigger t4 = TriggerBuilder.newTrigger().withIdentity("myCronTrigger").withSchedule(CronScheduleBuilder.cronSchedule("0 47 12 1/1 * ? *").inTimeZone(TimeZone.getTimeZone("GMT 8"))).build();
		// 通過schedulerFactory獲取一個排程器 
		Scheduler sc = StdSchedulerFactory.getDefaultScheduler();
		// 把作業和觸發器註冊到任務排程中
		sc.scheduleJob(job, t3);
		sc.start();
	}
}
