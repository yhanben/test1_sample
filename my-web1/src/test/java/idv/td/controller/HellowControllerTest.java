package idv.td.controller;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
//import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import idv.td.vo.UserInputVo;
import idv.td.web.WebApplication;

//import static org.junit.Assert.*;

//import org.junit.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//@SpringBootApplication(scanBasePackages = "idv.td")
//@SpringApplicationConfiguration(classes = WebApplication.class)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ImportResource(value = { "classpath:application-bean.xml" })
public class HellowControllerTest {
	
	
	 @Autowired
	 private TestRestTemplate restTemplate;
	
	@Test
	public void sayHelloWorld() {
		
		List<UserInputVo> listVo = new ArrayList<UserInputVo>();
		UserInputVo v1 = new UserInputVo();
		v1.setAge("23");
		v1.setPassworld("1111");
		v1.setUserName("userName");
		listVo.add(v1);
		
		UserInputVo v2 = new UserInputVo();
		v2.setAge("26");
		v2.setPassworld("1122");
		v2.setUserName("userName3");
		listVo.add(v2);
		
		UserInputVo v3 = new UserInputVo();
		v3.setAge("36");
		v3.setPassworld("55");
		v3.setUserName("userName5");
		listVo.add(v3);
		
		listVo.stream().filter(m -> m.getPassworld().length() >= 2).forEach(s -> System.out.print(s.getAge()));
		
		
		System.out.println("1111");
		//fail("Not yet implemented");
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromPath("/hello");
        Map<String, Object> uriParams = new HashMap<String, Object>();
        //uriParams.put("bookid", book.getBookid());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(
                builder.buildAndExpand(uriParams).toUri().toString(),
                HttpMethod.GET, entity, String.class);
        assertTrue("testGetBook Fail:\n" + response.getBody(),
                response.getStatusCode().is2xxSuccessful());
		
	}

}
