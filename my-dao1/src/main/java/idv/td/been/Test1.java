package idv.td.been;

import java.io.Serializable;

import lombok.Data;

//@Data
public class Test1 implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 流水序號 */
	private Long id;
	/** 姓名 */
	private String name;
	/** 電話 */
	private String phone;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
}
