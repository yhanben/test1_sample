package idv.td.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import idv.td.been.Test1;


//@Mapper
public interface Test1Mapper {

	/**
     * 通过名字查询用户信息
     */
    //@Select("SELECT * FROM test1 WHERE name = #{name}")
    //Test1 findUserByName(@Param("name") String name);

    /**
     * 查询所有用户信息
     */
    //@Select("SELECT * FROM test1")
    List<Test1> findAllUser();
	
}
