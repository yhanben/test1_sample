package idv.td.validation;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import idv.td.annotation.ListNotEmpty;


public class ListNotEmptyValidator implements ConstraintValidator<ListNotEmpty, List<?>> {
	@Override
	public void initialize(ListNotEmpty constraintAnnotation) {
	
	}

	@Override
	public boolean isValid(List<?> value, ConstraintValidatorContext context) {
		if( value == null || value.isEmpty() ) return false;
		  return true;
	}
}
