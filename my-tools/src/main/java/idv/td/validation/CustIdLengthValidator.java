package idv.td.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import idv.td.annotation.CustIdLength;

public class CustIdLengthValidator implements ConstraintValidator<CustIdLength, String> {

	@Override
	public void initialize(CustIdLength constraintAnnotation) {

	}

	@Override
	public boolean isValid(String custId, ConstraintValidatorContext ctx) {
		if (custId == null) {
			return true;
		}

		if (custId.length() == 10) {
			return true;
		}

		return false;
	}

}
