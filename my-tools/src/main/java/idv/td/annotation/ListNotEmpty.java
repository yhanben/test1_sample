package idv.td.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import idv.td.validation.ListNotEmptyValidator;

@Documented
@Constraint(validatedBy = ListNotEmptyValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD } )
@Retention(RetentionPolicy.RUNTIME)


public @interface ListNotEmpty {
	
	String message() default "Empty list";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
}
