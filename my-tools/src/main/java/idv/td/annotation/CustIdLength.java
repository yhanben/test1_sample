package idv.td.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.ReportAsSingleViolation;

import idv.td.validation.CustIdLengthValidator;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = { CustIdLengthValidator.class })
@ReportAsSingleViolation
public @interface CustIdLength {
	
		String message() default "custId length should be 10";

	  Class[] groups() default {};

	  Class[] payload() default {};
	
}
