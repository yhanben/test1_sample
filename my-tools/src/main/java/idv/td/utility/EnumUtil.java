package idv.td.utility;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;


public class EnumUtil {
	
	public static <T extends Enum<T>> T getEnum(Class<T> enumClass, CharSequence key) {
		Objects.requireNonNull(enumClass, "[enumClass] is null");
		//if(ValidateUtil.i)
		if(StringUtils.isEmpty(key)) {
			return null;
		}
		
		for(T en : enumClass.getEnumConstants()) {
			if(en.name().equals(key)) {
				return en;
			}
		}
		return null;
	}
	
}
