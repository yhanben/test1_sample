package idv.td.utility;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class ValidateUtil {
	
	private ValidateUtil() {}
	
	/**
	 * 檢查List是否為空
	 * 
	 * @param datas
	 * @return true:為空
	 * */
	public static boolean isEmpty(List<?> datas) {
		return datas == null || datas.isEmpty();
	}
	
	/**
	 * 檢查陣列是否為空
	 * 
	 * @param datas
	 * @return true:為空
	 * */
	public static boolean isEmpty(CharSequence... strs) {
		return strs == null || strs.length == 0;
	}
	
	public static boolean isEmpty(CharSequence str) {
		return str == null || str.length() == 0;
	}
	
	public static boolean StringIsBlank(String value) {
		
		return StringUtils.isBlank(value);
		
	}

}
