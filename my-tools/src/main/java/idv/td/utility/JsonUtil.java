package idv.td.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {

	/**
	 * 物件轉Json字串
	 * 
	 * 
	 * @return json字串
	 * */
	public static String toJson(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(obj);
		}catch(JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * json 轉物件
	 * 
	 * <pre>
	 * ex: ApprDoc apprDoc = JsonUtil.fromJson()("{\"name\":\"abc\"}",ApprDoc.clsss);
	 * </pre>
	 * 
	 * */
	public static <T> T fromJson(String json, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, clazz);
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
		
		
	}
	
	
}
